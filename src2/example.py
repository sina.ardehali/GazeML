"""
Demonstration of the GazeTracking library.
Check the README.md for complete documentation.
"""

import cv2
from random import randrange, uniform
from collections import deque, Counter
from gaze_tracking import GazeTracking

gaze = GazeTracking()
webcam = cv2.VideoCapture(0)
webcam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
webcam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
webcam.set(cv2.CAP_PROP_FPS, 60)

new_point = True
irand = 1
last_n_frames = deque([0]*5)

while True:

    # We get a new frame from the webcam
    _, frame = webcam.read()

    # We send this frame to GazeTracking to analyze it
    gaze.refresh(frame)

    frame = gaze.annotated_frame()
    text = ""
    bgr = frame
    text = ""

    if bgr is None:
        width_block = 0
        height_block   = 0
        h = 0
        w =0

    if bgr is not None:
        h, w, c = bgr.shape
        width_block = w // 3
        height_block   = h //3

        pxstep = w//3
        pystep = h//3
        line_color=(0, 255, 0)
        thickness=1
        type_= cv2.LINE_AA
        img = bgr
        x = pxstep
        y = pystep
        while x < img.shape[1]:
            cv2.line(img, (x, 0), (x, img.shape[0]), color=line_color, lineType=type_, thickness=thickness)
            x += pxstep

        while y < img.shape[0]:
            cv2.line(img, (0, y), (img.shape[1], y), color=line_color, lineType=type_, thickness=thickness)
            y += pystep



        if gaze.is_blinking():
            text = "Blinking"
            if irand == Counter(last_n_frames).most_common()[0][0]:
            # if irand == mode(last_n_frames):
                new_point = True
        elif gaze.is_right():
            if gaze.is_top():
                text = "En haut, droite"
                last_n_frames.popleft()
                last_n_frames.append(1)
                tp = 2
                cv2.rectangle(bgr,(tp,tp),(tp+width_block,tp+height_block),(255,0,0),3)
            # zone 7
            elif gaze.is_bottom():
                text = "En bas, droite"
                last_n_frames.popleft()
                last_n_frames.append(7)
                tp = 2
                cv2.rectangle(bgr,(2,2*height_block),(width_block,h-2),(255,0,0),3)
            # zone 4
            else:
                last_n_frames.popleft()
                last_n_frames.append(4)
                text = "Au centre, droite"
                cv2.rectangle(bgr,(2,height_block),(width_block,2*height_block),(255,0,0),3)
        elif gaze.is_left():
            # text = "Looking left"
            if gaze.is_top():
                last_n_frames.popleft()
                last_n_frames.append(3)
                text = "En haut, gauche"
                cv2.rectangle(bgr,(2*width_block,2),(w-2,height_block),(255,0,0),3)
            # zone 9
            elif gaze.is_bottom():
                last_n_frames.popleft()
                last_n_frames.append(9)
                text = "En bas, gauche"
                cv2.rectangle(bgr,(2*width_block,2*height_block),(w-2,h-2),(255,0,0),3)
            # zone 6
            else:
                text = "Au centre, gauche"
                last_n_frames.popleft()
                last_n_frames.append(6)
                cv2.rectangle(bgr,(2*width_block,height_block),(w-2,2*height_block),(255,0,0),3)

        elif gaze.is_center():
            # text = "Looking center"
            if gaze.is_top():
                text = "En haut"
                last_n_frames.popleft()
                last_n_frames.append(2)
                cv2.rectangle(bgr,(width_block,2),(2*width_block,height_block),(255,0,0),3)
            # zone 8
            elif gaze.is_bottom():
                text = "En bas"
                print(text)
                last_n_frames.popleft()
                last_n_frames.append(8)
                cv2.rectangle(bgr,(width_block,2*height_block),(2*width_block,h-2),(255,0,0),3)
            # zone 5
            else:
                text = "Au centre"
                print(text)
                last_n_frames.popleft()
                last_n_frames.append(5)
                if width_block and height_block:
                    cv2.rectangle(bgr,(width_block,height_block),(2*width_block,2*height_block),(255,0,0),3)



        most_regarded = Counter(last_n_frames).most_common()[0][0]


        if most_regarded == 1:
            tp = 2
            cv2.rectangle(bgr,(tp,tp),(tp+width_block,tp+height_block),(255,0,255),3)
        elif most_regarded == 7:
            tp = 2
            cv2.rectangle(bgr,(2,2*height_block),(width_block,h-2),(255,0,255),3)       
        elif most_regarded == 4:
            cv2.rectangle(bgr,(2,height_block),(width_block,2*height_block),(255,0,255),3)
        elif most_regarded == 3:
            cv2.rectangle(bgr,(2*width_block,2),(w-2,height_block),(255,0,255),3)
        elif most_regarded == 9:
            cv2.rectangle(bgr,(2*width_block,2*height_block),(w-2,h-2),(255,0,255),3)
        elif most_regarded == 6:
            cv2.rectangle(bgr,(2*width_block,height_block),(w-2,2*height_block),(255,0,255),3)
        elif most_regarded == 2:
            cv2.rectangle(bgr,(width_block,2),(2*width_block,height_block),(255,0,255),3)
        elif most_regarded == 8:
            cv2.rectangle(bgr,(width_block,2*height_block),(2*width_block,h-2),(255,0,255),3)
        elif most_regarded == 5:
            cv2.rectangle(bgr,(width_block,height_block),(2*width_block,2*height_block),(255,0,255),3)



        cv2.putText(frame, text, (90, 60), cv2.FONT_HERSHEY_DUPLEX, 1.6, (147, 58, 31), 2)

        left_pupil = gaze.pupil_left_coords()
        right_pupil = gaze.pupil_right_coords()
        cv2.putText(frame, "Left pupil:  " + str(left_pupil), (90, 130), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)
        cv2.putText(frame, "Right pupil: " + str(right_pupil), (90, 165), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)


        if new_point == True:
            irand = randrange(1, 10)

        img = bgr   
        radius = 4
        color=(0, 0, 255)
        tp = 2
        if irand == 1:
            # print('iciiiiiiiiiiiii1')
            center_x = (tp+width_block+tp)//2
            center_y = (tp+height_block+tp)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 2:
            # print('iciiiiiiiiiiiii2')
            center_x = (2*width_block+width_block)//2
            center_y = (height_block+2)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 3:
            # print('iciiiiiiiiiiiii3')
            center_x = (2*width_block+w-2)//2
            center_y = (2+height_block)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 4:
            # print('iciiiiiiiiiiiii4')
            center_x = (width_block+2)//2
            center_y = (2*height_block+height_block)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 5:
            # print('iciiiiiiiiiiiii5')
            center_x = (width_block+2*width_block)//2
            center_y = (height_block+2*height_block)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 6:
            # print('iciiiiiiiiiiiii6')
            center_x = (2*width_block+w-2)//2
            center_y = (2*height_block+height_block)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 7:
            # print('iciiiiiiiiiiiii7')
            center_x = (tp+width_block)//2
            center_y = (2*height_block+h-2)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 8:
            # print('iciiiiiiiiiiiii8')
            center_x = (width_block+2*width_block)//2
            center_y = (2*height_block+h-2)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        elif irand == 9:
            # print('iciiiiiiiiiiiii9')
            center_x = (2*width_block+w-2)//2
            center_y = (2*height_block+h-2)//2
            center = (center_x, center_y)
            cv2.circle(frame, center, radius, color, thickness=4, lineType=8, shift=0)
        else:
            print("rien du tout")
        
        new_point = False


        cv2.imshow("Demo", frame)

        if cv2.waitKey(1) == 27:
            break
