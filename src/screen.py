import numpy as np
import cv2
import pyautogui
from collections import deque, Counter

RES_SCREEN = pyautogui.size() # RES_SCREEN[0] -> width
                              # RES_SCREEN[1] -> heigth

last_n_frames = deque([0]*200)

class Screen:

    def __init__(self, width=1280, height=720, frame= np.ones((1280, 720, 3))):
        self.width = width
        self.height = height

        # ajotué par Sina
        self.width_block = self.width // 3
        self.height_block   = self.height //3
        self.last_n_frames = deque([0] * 200)
        

        self.pointer = (0,0)
        self.mode = "normal"
        self.screen = frame
        self.clean()

    def refresh(self):
        self.clean()
        self.draw_pointer()
        self.show()

    def update(self, gaze):
        self.pointer = gaze

    def clean(self):
        # si je veux accumuler les points , je peux supprimer la ligne ci-desosus
        self.screen = np.ones((self.height, self.width, 3))
        self.print_instructions()

    def draw(self, point, progress=0):
        x, y = point
        if progress == 1.0:
            cv2.circle(self.screen, (x, y), 5, (0, 255, 0), -1)
        else:
            cv2.circle(self.screen, (x, y), 5, (0, 0, 0), -1)

        if progress > 0:
            # Ellipse parameters
            radius = 7
            axes = (radius, radius)
            angle = 0
            start_angle = 0
            end_angle = 360 * progress
            cv2.ellipse(self.screen, (x, y), axes, angle, start_angle, end_angle, (0, 255, 0), 2)


    def draw_center(self):
        x, y = (int(0.5 * self.width), int(0.5 * self.height))
        cv2.circle(self.screen, (x, y), 5, (0, 0, 0), -1)

    def draw_pointer(self):
        x, y = self.pointer


        w = RES_SCREEN[0]
        h = RES_SCREEN[1]
        tp = 2
   
        if (x <=  self.width_block):
            ##1
            if (y <= self.height_block):
                cv2.rectangle(self.screen,(tp,tp),(tp+self.width_block,tp+self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(1)
            ##4
            elif (y <= 2*self.height_block):
                cv2.rectangle(self.screen,(2,self.height_block),(self.width_block,2*self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(4)
            ##7
            else:
                cv2.rectangle(self.screen,(2,2*self.height_block),(self.width_block,h-2),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(7)
        elif (x <= 2*self.width_block):
            ##2
            if (y <= self.height_block):
                cv2.rectangle(self.screen,(self.width_block,2),(2*self.width_block,self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(2)
            ##5
            elif ( y <= 2*self.height_block):
                cv2.rectangle(self.screen,(self.width_block,self.height_block),(2*self.width_block,2*self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(5)
            ##8
            else:
                cv2.rectangle(self.screen,(self.width_block,2*self.height_block),(2*self.width_block,h-2),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(8)
        else:
            ##3
            if (y <= self.height_block):
                cv2.rectangle(self.screen,(2*self.width_block,2),(w-2,self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(3)
            ##6
            elif (y <= 2*self.height_block):
                cv2.rectangle(self.screen,(2*self.width_block,self.height_block),(w-2,2*self.height_block),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(6)
            ##9
            else:
                cv2.rectangle(self.screen,(2*self.width_block,2*self.height_block),(w-2,h-2),(255,0,0),3)
                self.last_n_frames.popleft()
                self.last_n_frames.append(9)

        
        most_regarded = Counter(last_n_frames).most_common()[0][0]


        dict_most_regarded = {
            1: "self.screen,(2,2),(2+width_block,2+height_block),(255,0,255),3",
            2: "self.screen,(width_block,2),(2*width_block,height_block),(255,0,255),3",
            3: "self.screen,(2*width_block,2),(w-2,height_block),(255,0,255),3",
            4: "self.screen,(2,height_block),(width_block,2*height_block),(255,0,255),3",
            5: "self.screen,(width_block,height_block),(2*width_block,2*height_block),(255,0,255),3",
            6: "self.screen,(2*width_block,height_block),(w-2,2*height_block),(255,0,255),3",
            7: "self.screen,(2,2*height_block),(width_block,h-2),(255,0,255),3",
            8: "self.screen,(width_block,2*height_block),(2*width_block,h-2),(255,0,255),3",
            9: "self.screen,(2*width_block,2*height_block),(w-2,h-2),(255,0,255),3",
        }

        if most_regarded:
            res = dict_most_regarded[most_regarded]
            print("res : ", res)
            cv2.rectangle(res)

        cv2.circle(self.screen, (x, y), 5, (0, 255, 0), -1)

    def print_instructions(self):
        x, y0, dy = int(0.03 * self.width), int(0.8 * self.height), 35

        if self.mode == "normal":
            instructions = "Appuyez:\nESC pour quitter\nc pour commencer la calibration"
        if self.mode == "calibration":
#            instructions = "Press:\nESC to terminate\nn to next calibration step"
            instructions = "Appuyer:\nESC pour terminer la calibration"

        for i, line in enumerate(instructions.split('\n')):
            y = y0 + i*dy
            cv2.putText(img=self.screen, text=line, org=(x, y),fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0), thickness=2)


    def print_message(self, msg):

        font = cv2.FONT_HERSHEY_SIMPLEX
        fs = 2
        th = 3

        for i, line in enumerate(msg.split('\n')):
            textsize = cv2.getTextSize(line, font, fs, th)[0]
            x = (self.width - textsize[0]) // 2
            y0, dy = (self.height + textsize[1]) // 2, textsize[1] + 30

            y = y0 + i*dy
            cv2.putText(img=self.screen, text=line, org=(x, y),fontFace=font, fontScale=fs, color=(0,0,0), thickness=th)


    def show(self):
        cv2.namedWindow("screen")
        cv2.moveWindow("screen", int(RES_SCREEN[0] / 2 - self.width/2), 0)
        cv2.imshow("screen", self.screen)
